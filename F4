provider "aws" {
  region = "votre_region_aws"
}

data "aws_iam_policy_document" "emr_autoscaling_policy" {
  source_json = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "elasticmapreduce:DescribeJobFlows",
        "elasticmapreduce:ListClusters",
        "elasticmapreduce:ListInstanceFleets",
        "elasticmapreduce:ListInstances",
        "elasticmapreduce:ListSteps",
        "elasticmapreduce:ModifyInstanceFleet",
        "elasticmapreduce:ModifyInstanceGroups",
        "elasticmapreduce:RunJobFlow",
        "elasticmapreduce:SetTerminationProtection",
        "elasticmapreduce:TerminateJobFlows",
        "ec2:AuthorizeSecurityGroupIngress",
        "ec2:CreateSecurityGroup",
        "ec2:CreateTags",
        "ec2:DeleteSecurityGroup",
        "ec2:DescribeSecurityGroups",
        "ec2:DescribeTags",
        "ec2:RevokeSecurityGroupIngress"
      ],
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_role" "emr_autoscaling_role" {
  name = "emr_autoscaling_role"
  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = "sts:AssumeRole",
        Effect = "Allow",
        Principal = {
          Service = "elasticmapreduce.amazonaws.com"
        }
      }
    ]
  })
}

resource "aws_iam_role_policy_attachment" "emr_autoscaling_attachment" {
  role       = aws_iam_role.emr_autoscaling_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonElasticMapReduceforEC2Role"
}

resource "aws_iam_policy" "emr_autoscaling_policy" {
  name        = "emr_autoscaling_policy"
  description = "Policy for EMR Autoscaling"
  policy      = data.aws_iam_policy_document.emr_autoscaling_policy.json
}

resource "aws_iam_role_policy_attachment" "emr_autoscaling_policy_attachment" {
  policy_arn = aws_iam_policy.emr_autoscaling_policy.arn
  role       = aws_iam_role.emr_autoscaling_role.name
}
