# modules/emr-cluster/outputs.tf

output "cluster_id" {
  description = "The ID of the created EMR cluster."
  value       = aws_emr_cluster.cluster.id
}

output "master_dns" {
  description = "The DNS name of the master node in the EMR cluster."
  value       = aws_emr_cluster.cluster.master_public_dns
}