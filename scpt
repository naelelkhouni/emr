#!/bin/bash
if grep isMaster /mnt/var/lib/info/instance.json | grep true;
then        
    sudo chmod -R g+rw /var/log/hbase
    sudo chmod -R g+rw /var/log/spark
    sudo chmod -R g+rw /var/log/hive
    sudo usermod -a -G hbase hadoop
    sudo usermod -a -G spark hadoop
    sudo usermod -a -G hive hadoop
    exit 0
fi