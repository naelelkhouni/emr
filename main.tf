# modules/emr-cluster/main.tf

resource "aws_emr_cluster" "cluster" {
  name                             = "${var.name_prefix}-emr-cluster"
  release_label                    = var.release_label
  applications                     = var.applications
  additional_info                  = var.additional_info
  termination_protection           = var.termination_protection
  keep_job_flow_alive_when_no_steps = var.keep_job_flow_alive_when_no_steps

  ec2_attributes {
    subnet_id                        = var.subnet_id
    key_name                         = var.key_name
    emr_managed_master_security_group = var.emr_master_security_group
    emr_managed_slave_security_group  = var.emr_slave_security_group
    instance_profile                 = var.emr_ec2_instance_profile
  }

  master_instance_group {
    name           = "${var.name_prefix}-master"
    instance_type  = var.master_instance_type
    instance_count = var.master_instance_count

    ebs_config {
      size                 = var.master_ebs_size
      type                 = var.master_ebs_type
      volumes_per_instance = var.master_volumes_per_instance
    }
  }

  core_instance_group {
    name           = "${var.name_prefix}-core"
    instance_type  = var.core_instance_type
    instance_count = var.core_instance_count

    ebs_config {
      size                 = var.core_ebs_size
      type                 = var.core_ebs_type
      volumes_per_instance = var.core_volumes_per_instance
    }
  }

  tags = var.tags

  service_role     = var.emr_service_role
  autoscaling_role = var.emr_autoscaling_role
}
