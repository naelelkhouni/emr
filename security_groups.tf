# modules/emr-cluster/security_group.tf

resource "aws_security_group" "emr_master_sg" {
  name        = "${var.name_prefix}-emr-master-sg"
  description = "${var.name_prefix} Security group for EMR master node"

  ingress {
    from_port   = 0
    to_port     = 65535
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}

resource "aws_security_group" "emr_slave_sg" {
  name        = "${var.name_prefix}-emr-slave-sg"
  description = "${var.name_prefix} Security group for EMR slave nodes"

  ingress {
    from_port   = 0
    to_port     = 65535
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}
