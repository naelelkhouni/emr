# modules/emr-cluster/variables.tf

variable "name_prefix" {
  description = "A prefix used for naming resources."
  type = string
  default = ""
}

variable "name" {
  description = "The name of the EMR cluster."
}

variable "release_label" {
  description = "The release label for the EMR cluster."
}

variable "applications" {
  description = "The list of applications to be installed on the EMR cluster."
}

variable "additional_info" {
  description = "Additional information for EMR cluster configuration."
}

variable "termination_protection" {
  description = "Whether to enable termination protection for the EMR cluster."
  default     = false
}

variable "keep_job_flow_alive_when_no_steps" {
  description = "Whether to keep the EMR cluster alive when no steps are running."
  default     = true
}

variable "subnet_id" {
  description = "The ID of the subnet in which to launch the EMR cluster."
}

variable "key_name" {
  description = "The name of the EC2 key pair to use for the EMR cluster instances."
}

variable "emr_master_security_group" {
  description = "The security group for the EMR master node."
}

variable "emr_slave_security_group" {
  description = "The security group for EMR slave nodes."
}

variable "emr_ec2_instance_profile" {
  description = "The name or ARN of the IAM instance profile for EMR cluster instances."
}

variable "master_instance_name" {
  description = "The name of the master instance group."
}

variable "master_instance_type" {
  description = "The EC2 instance type for the master node."
}

variable "master_instance_count" {
  description = "The number of instances in the master instance group."
}

variable "master_ebs_size" {
  description = "The size of the EBS volume for the master node."
}

variable "master_ebs_type" {
  description = "The type of EBS volume for the master node."
}

variable "master_volumes_per_instance" {
  description = "The number of EBS volumes to attach to each instance in the master instance group."
}

variable "core_instance_name" {
  description = "The name of the core instance group."
}

variable "core_instance_type" {
  description = "The EC2 instance type for the core nodes."
}

variable "core_instance_count" {
  description = "The number of instances in the core instance group."
}

variable "core_ebs_size" {
  description = "The size of the EBS volume for the core nodes."
}

variable "core_ebs_type" {
  description = "The type of EBS volume for the core nodes."
}

variable "core_volumes_per_instance" {
  description = "The number of EBS volumes to attach to each instance in the core instance group."
}

variable "emr_service_role" {
  description = "The IAM role for the EMR service."
}

variable "emr_autoscaling_role" {
  description = "The IAM role for EMR autoscaling."
}


variable "tags" {
  description = "A map of tags (key-value pairs) passed to resources"
  type = map(string)
  default = {}
}