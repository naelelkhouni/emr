provider "aws" {
  region = "us-west-2"
}

data "aws_vpc" "selected" {
  id = var.vpc_id
}

resource "aws_emr_cluster" "this" {
  for_each = var.emr_clusters

  additional_info = each.value.additional_info
  applications    = var.common_emr_config.applications

  dynamic "auto_termination_policy" {
    for_each = length(each.value.auto_termination_policy) > 0 ? [each.value.auto_termination_policy] : []

    content {
      idle_timeout = try(auto_termination_policy.value.idle_timeout, null)
    }
  }

  autoscaling_role = local.create_autoscaling_iam_role ? aws_iam_role.autoscaling[0].arn : var.autoscaling_iam_role_arn

  dynamic "bootstrap_action" {
    for_each = each.value.bootstrap_action

    content {
      args = try(bootstrap_action.value.args, null)
      name = bootstrap_action.value.name
      path = bootstrap_action.value.path
    }
  }

  configurations      = var.common_emr_config.configurations
  configurations_json = var.common_emr_config.configurations_json

  dynamic "core_instance_fleet" {
    for_each = length(each.value.core_instance_fleet) > 0 ? [each.value.core_instance_fleet] : []

    content {
      dynamic "instance_type_configs" {
        for_each = try(core_instance_fleet.value.instance_type_configs, [])

        content {
          bid_price                                  = try(instance_type_configs.value.bid_price, null)
          bid_price_as_percentage_of_on_demand_price = try(instance_type_configs.value.bid_price_as_percentage_of_on_demand_price, 60)

          dynamic "configurations" {
            for_each = try(instance_type_configs.value.configurations, [])

            content {
              classification = try(configurations.value.classification, null)
              properties     = try(configurations.value.properties, null)
            }
          }

          dynamic "ebs_config" {
            for_each = try(instance_type_configs.value.ebs_config, [])

            content {
              iops                 = try(ebs_config.value.iops, null)
              size                 = try(ebs_config.value.size, 64)
              type                 = try(ebs_config.value.type, "gp3")
              volumes_per_instance = try(ebs_config.value.volumes_per_instance, null)
            }
          }

          instance_type     = instance_type_configs.value.instance_type
          weighted_capacity = try(instance_type_configs.value.weighted_capacity, null)
        }
      }

      dynamic "launch_specifications" {
        for_each = try([core_instance_fleet.value.launch_specifications], [])

        content {
          dynamic "on_demand_specification" {
            for_each = try([launch_specifications.value.on_demand_specification], [])

            content {
              allocation_strategy = try(on_demand_specification.value.allocation_strategy, "lowest-price")
            }
          }

          dynamic "spot_specification" {
            for_each = try([launch_specifications.value.spot_specification], [])

            content {
              allocation_strategy      = try(spot_specification.value.allocation_strategy, "capacity-optimized")
              block_duration_minutes   = try(launch_specifications.value.spot_specification.block_duration_minutes, null)
              timeout_action           = try(launch_specifications.value.spot_specification.timeout_action, "SWITCH_TO_ON_DEMAND")
              timeout_duration_minutes = try(launch_specifications.value.spot_specification.timeout_duration_minutes, 60)
            }
          }
        }
      }

      name                      = "${each.key}-core-fleet"
      target_on_demand_capacity = try(core_instance_fleet.value.target_on_demand_capacity, null)
      target_spot_capacity      = try(core_instance_fleet.value.target_spot_capacity, null)
    }
  }

  dynamic "core_instance_group" {
    for_each = length(each.value.core_instance_group) > 0 ? [each.value.core_instance_group] : []

    content {
      autoscaling_policy = try(core_instance_group.value.autoscaling_policy, null)
      bid_price          = try(core_instance_group.value.bid_price, null)

      dynamic "ebs_config" {
        for_each = try(core_instance_group.value.ebs_config, [])

        content {
          iops                 = try(ebs_config.value.iops, null)
          size                 = try(ebs_config.value.size, 64)
          throughput           = try(ebs_config.value.throughput, null)
          type                 = try(ebs_config.value.type, "gp3")
          volumes_per_instance = try(ebs_config.value.volumes_per_instance, null)
        }
      }

      instance_count = try(core_instance_group.value.instance_count, null)
      instance_type  = core_instance_group.value.instance_type
      name           = "${each.key}-core-group"
    }
  }

  custom_ami_id        = each.value.custom_ami_id
  ebs_root_volume_size = var.common_emr_config.ebs_root_volume_size

  dynamic "ec2_attributes" {
    for_each = length(each.value.ec2_attributes) > 0 || local
